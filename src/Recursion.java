
public class Recursion {
    
    public static int powerN(int num, int power) {
        if (power == 1) {
            return num;
        }
        return num * powerN(num, power-1);
    }
    
    public static int triangle(int row) {
        if (row == 0) {
            return 0;
        }
        return row + triangle(row - 1);
    }
    
    public static void main(String[] args) {
        System.out.println("Hi");
        System.out.println(triangle(2));
    }

}

